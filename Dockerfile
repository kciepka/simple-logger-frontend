FROM ubuntu

RUN apt-get update && apt-get install nginx --assume-yes --no-install-recommends

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html

COPY ./build/ .

EXPOSE 443 80

CMD nginx -g 'daemon off;'