import React, { Component } from 'react';
import { Button } from '@rmwc/button';
import { Grid, GridCell, GridInner } from '@rmwc/grid';
import Dashboard from './Dashboard';
import AppDetails from './AppDetails';
import NewApp from './NewApp';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import * as queryString from 'query-string';
import * as axios from 'axios';
import './App.css';

const config = require('./config.json');

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { apps: [] }
    this.checkToken();
  }

  checkToken() {
    const parsed = queryString.parse(window.location.search);
    if (parsed['access_token']) {
      localStorage.setItem('token', parsed['access_token'])
    }

    if (!localStorage.getItem('token')) {
      window.location.href = `${config.backendUrl}/auth/google/login?redirect_uri=http://simplelogger.ciepka.com`
    }
  }

  componentDidMount() {
    this.loadApps();
  }

  loadApps = () => {
    axios.get(`${config.backendUrl}/api/applications`, {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(resp => {
        this.setState({ apps: resp.data });
      })
      .catch(err => {
        console.error(err);
      })
  }

  onDeleteApp = () => {
    this.loadApps();
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Grid>
            <GridInner>
              <GridCell span="3">
                <Link to="/dashboard"><Button raised>Dashboard</Button></Link>
                {
                  this.state.apps.map(app => {
                    const link = "/apps/" + app.id;
                    return <Link key={app.id} to={link}><Button raised>{app.name}</Button></Link >
                  })
                }
                <GridInner className="add-app" align="right">
                  <GridCell span="12">
                    <Link to="/new-app"> <Button icon="add" raised theme={['secondaryBg', 'onSecondary']} label="Add new app">Add new app</Button></Link>
                  </GridCell>
                </GridInner>
              </GridCell>
              <GridCell span="9">
                <Switch>
                  <Route exact path="/dashboard" component={Dashboard} />
                  <Route exact path="/new-app" render={props => <NewApp onSubmit={this.loadApps} {...props} />} />
                  {
                    this.state.apps.map(app => {
                      const link = "/apps/" + app.id;
                      return <Route key={app.id} exact path={link} render={props => <AppDetails onDelete={this.onDeleteApp} app={app} {...props} />} />
                    })
                  }
                  <Route path="*" component={Dashboard} />
                </Switch>
              </GridCell>
            </GridInner>
          </Grid>
        </Router>
      </div>
    );
  }
}

export default App;
