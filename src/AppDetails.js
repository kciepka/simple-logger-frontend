import React from 'react';
import { TabBar, Tab } from '@rmwc/tabs';
import './AppDetails.css';
import AppInfo from './app-details/AppInfo';
import AppLogs from './app-details/AppLogs';
import AppSettings from './app-details/AppSettings';

class AppDetails extends React.Component {

    constructor() {
        super();
        this.state = { activeTab: 0, component: null }
    }

    selectTab(index) {
        switch (index) {
            case 0:
                return <AppInfo app={this.props.app} />;
            case 1:
                return <AppLogs app={this.props.app} />;
            case 2:
                return <AppSettings onDelete={this.props.onDelete} app={this.props.app} />;
            default:
                return <AppInfo app={this.props.app} />;
        }
    }

    render() {
        return (
            <>
                <h2>{this.props.app.name}</h2>
                <TabBar
                    activeTabIndex={this.state.activeTab}
                    onActivate={evt => this.setState(
                        {
                            activeTab: evt.detail.index,
                            component: this.selectTab(evt.detail.index)
                        })}>
                    <Tab className="tab">Info</Tab>
                    <Tab className="tab">Logs</Tab>
                    <Tab className="tab">Settings</Tab>
                </TabBar>
                {this.state.component}
            </>
        )
    }
}

export default AppDetails;