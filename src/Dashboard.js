import React, { Component } from 'react';
import {
    DataTable,
    DataTableContent,
    DataTableHead,
    DataTableBody,
    DataTableHeadCell,
    DataTableRow,
    DataTableCell
} from '@rmwc/data-table';
import { distanceInWordsToNow } from 'date-fns';
import '@rmwc/data-table/data-table.css';
import axios from 'axios';
import './Dashboard.css';

const config = require('./config.json');

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = { recent: [] }
    }

    loadLogs() {
        return axios.get(`${config.backendUrl}/api/logs/recent?limit=8`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(resp => {
                this.setState({
                    recent: resp.data
                });
            })
            .catch(err => {
                console.error(err);
            })
    }

    componentDidMount() {
        this.loadLogs();
    }

    render() {
        return (
            <>
                <div>
                    <h2>Recent logs</h2>
                    <DataTable>
                        <DataTableContent>
                            <DataTableHead>
                                <DataTableRow>
                                    <DataTableHeadCell>Time</DataTableHeadCell>
                                    <DataTableHeadCell
                                        alignStart>
                                        Message
                                    </DataTableHeadCell>
                                    <DataTableHeadCell alignStart>
                                        Level
                                    </DataTableHeadCell>
                                </DataTableRow>
                            </DataTableHead>
                            <DataTableBody>
                                {
                                    this.state.recent.map(log => {
                                        return (
                                            <DataTableRow key={log.id}>
                                                <DataTableCell>{distanceInWordsToNow(new Date(log.timestamp), { includeSeconds: true })}</DataTableCell>
                                                <DataTableCell className="wrapped" alignStart>{log.value}</DataTableCell>
                                                <DataTableCell alignStart>{log.level}</DataTableCell>
                                            </DataTableRow>)
                                    })
                                }
                            </DataTableBody>
                        </DataTableContent>
                    </DataTable>
                </div>
            </>

        )
    }
}

export default Dashboard;