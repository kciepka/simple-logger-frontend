import React, { Component } from 'react';
import { TextField } from '@rmwc/textfield';
import { Grid, GridCell } from '@rmwc/grid';
import { Button } from '@rmwc/button';
import * as axios from 'axios';

const config = require('./config.json');

class NewApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            origin: ''
        }
    }

    createApp = () => {
        axios.post(`${config.backendUrl}/api/applications`, this.state, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(() => {
                this.props.onSubmit();
                this.props.history.push('/dashboard');
            })
            .catch((err) => {
                console.error(err);
            })
    }

    render() {
        return (
            <>
                <h2>Add new app</h2>
                <Grid>
                    <GridCell span="4">
                        <TextField label="Name" onChange={evt => this.setState({ name: evt.target.value })} />
                    </GridCell>
                    <GridCell span="4">
                        <TextField label="Origin" onChange={evt => this.setState({ origin: evt.target.value })} />
                    </GridCell>
                    <GridCell span="4">
                        <Button
                            label="Add"
                            raised
                            theme={['secondaryBg', 'onSecondary']}
                            onClick={this.createApp}
                        >Add</Button>
                    </GridCell>
                </Grid>
            </>
        )
    }
}

export default NewApp;