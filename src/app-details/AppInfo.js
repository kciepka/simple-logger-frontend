import React from 'react';
import { Grid, GridCell } from '@rmwc/grid';
import './AppInfo.css'
import { Button } from '@rmwc/button';
import * as axios from 'axios';
import { SimpleDialog } from '@rmwc/dialog';
import {
    DataTable,
    DataTableContent,
    DataTableBody,
    DataTableRow,
    DataTableCell
} from '@rmwc/data-table';
import Snackbar from '@material-ui/core/Snackbar';
import '@rmwc/data-table/data-table.css';
import { format } from 'date-fns';

const config = require('../config.json');

class AppInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            app: props.app,
            generateAPIKeyDialogOpen: false,
            error: {
                message: '',
                open: false
            }
        }
    }

    verifyOrigin = () => {
        return axios.post(`${config.backendUrl}/api/applications/${this.state.app.id}/verification`, null, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((resp) => {
                console.log('verified domain');
                this.setState(resp.data);

            })
            .catch(err => {
                console.error(err);
                this.setState({
                    error: {
                        message: 'Could not verify domain',
                        open: true
                    }
                })
            })
    }

    openGenerateAPIKeyDialog = () => {
        if (this.state.app.apiKey) {
            return this.setState({ generateAPIKeyDialogOpen: true })
        }
        else {
            return this.generateAPIKey();
        }
    }

    generateAPIKey = () => {
        return axios.post(`${config.backendUrl}/api/applications/${this.state.app.id}/apikey`, null, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((resp) => {
                console.log('created api key');
                this.setState({ app: resp.data });

            })
            .catch(err => {
                console.error(err);
            })
    }

    render() {
        return (
            <>
                <DataTable>
                    <DataTableContent>
                        <DataTableBody>
                            <DataTableRow>
                                <DataTableCell>Created At:</DataTableCell>
                                <DataTableCell alignStart>{format(new Date(this.state.app.createdAt), 'DD MMM YYYY hh:mm')}</DataTableCell>
                                <DataTableCell>Origin:</DataTableCell>
                                <DataTableCell alignStart>{this.state.app.origin}</DataTableCell>
                            </DataTableRow>
                            <DataTableRow>
                                <DataTableCell>Verification Code:</DataTableCell>
                                <DataTableCell alignStart>{this.state.app.originVerificationCode}</DataTableCell>
                                <DataTableCell>API Key:</DataTableCell>
                                <DataTableCell alignStart>{this.state.app.apiKey ? this.state.app.apiKey : 'None'}</DataTableCell>
                            </DataTableRow>
                        </DataTableBody>
                    </DataTableContent>
                </DataTable>
                <Grid>
                    <GridCell span="4">
                        {
                            this.state.app.originVerified ?
                                <Button
                                    raised disabled
                                >Verify origin</Button>
                                :
                                <Button
                                    raised
                                    onClick={this.verifyOrigin}
                                    theme={['secondaryBg', 'onSecondary']}>Verify origin</Button>
                        }
                        {
                            this.state.app.originVerified ?
                                <Button
                                    raised
                                    onClick={this.openGenerateAPIKeyDialog}
                                    theme={['secondaryBg', 'onSecondary']}>Generate API key</Button>
                                :
                                <Button
                                    disabled raised
                                >Generate API key</Button>
                        }
                    </GridCell>
                </Grid>
                <SimpleDialog
                    title="Generate API key"
                    body="Are you sure you want to re-generate API key? Old key will deactivate"
                    open={this.state.generateAPIKeyDialogOpen}
                    acceptLabel="Generate"
                    cancelLabel="Cancel"
                    onClose={evt => {
                        if (evt.detail.action === 'accept') {
                            this.generateAPIKey();
                        }
                        this.setState({ generateAPIKeyDialogOpen: false })
                    }}
                />
                <Snackbar
                    open={this.state.error.open}
                    onClose={() => { this.setState({ error: { message: '', open: false } }) }}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    autoHideDuration={2000}
                    message={<span id="message-id">{this.state.error.message}</span>}
                />
            </>
        )
    }
}

export default AppInfo;