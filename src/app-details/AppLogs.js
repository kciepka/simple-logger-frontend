import React from 'react';
import * as axios from 'axios';
import {
    DataTable,
    DataTableContent,
    DataTableHead,
    DataTableBody,
    DataTableHeadCell,
    DataTableRow,
    DataTableCell
} from '@rmwc/data-table';
import '@rmwc/data-table/data-table.css';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import { format } from 'date-fns';

const config = require('../config.json');

class AppLogs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            app: props.app,
            pageSize: 5,
            current: 1,
            total: 0,
            offset: 0,
            logs: []
        };
    }

    loadLogs(page) {
        const offset = parseInt(page) * this.state.pageSize - this.state.pageSize;
        axios.get(`${config.backendUrl}/api/logs?apiKey=${this.state.app.apiKey}&limit=${this.state.pageSize}&offset=${offset}`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(resp => {
                this.setState({
                    logs: resp.data.result,
                    total: resp.data.pagination.total,
                    offset: resp.data.pagination.offset

                });
            })
            .catch(err => {
                console.error(err);
            })
    }

    componentDidMount() {
        this.loadLogs(this.state.current);
    }

    handlePageChange = (page) => {
        this.setState({ current: page, total: this.state.total, offset: this.state.offset })
        this.loadLogs(page);
    }

    itemRender = (current, type, element) => {
        if (type === 'page') {
            return <a href={`#${current}`}>{current}</a>;
        }
        return element;
    };

    render() {
        return (
            <>
                <Pagination
                    //current={this.state.current}
                    itemRender={this.itemRender}
                    onChange={this.handlePageChange}
                    total={this.state.total}
                    pageSize={this.state.pageSize}
                />
                <DataTable>
                    <DataTableContent>
                        <DataTableHead>
                            <DataTableRow>
                                <DataTableHeadCell>Time</DataTableHeadCell>
                                <DataTableHeadCell
                                    alignStart
                                // sort={this.state.sortDir || null}
                                // onSortChange={sortDir => {
                                //     this.setState({ sortDir })
                                //     console.log(sortDir)
                                // }}
                                >
                                    Message
                                    </DataTableHeadCell>
                                <DataTableHeadCell alignStart>
                                    Level
                                    </DataTableHeadCell>
                            </DataTableRow>
                        </DataTableHead>
                        <DataTableBody>
                            {
                                this.state.logs.map(log =>
                                    <DataTableRow key={log.id}>
                                        <DataTableCell>{format(new Date(log.timestamp), 'DD MMM YYYY hh:mm:ss:SSS')}</DataTableCell>
                                        <DataTableCell className="wrapped main-cell" alignStart>{log.value}</DataTableCell>
                                        <DataTableCell alignStart>{log.level}</DataTableCell>
                                    </DataTableRow>

                                )
                            }

                        </DataTableBody>
                    </DataTableContent>
                </DataTable>
            </>
        )
    }
}

export default AppLogs;