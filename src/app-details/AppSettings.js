import React from 'react';
import { Grid, GridCell } from '@rmwc/grid';
import { Select } from '@rmwc/select';
import { Switch } from '@rmwc/switch';
import * as axios from 'axios';
import { Button } from '@rmwc/button';
import {
    DataTable,
    DataTableContent,
    DataTableBody,
    DataTableRow,
    DataTableCell
} from '@rmwc/data-table';
import { SimpleDialog } from '@rmwc/dialog';
import '@rmwc/data-table/data-table.css';
import './AppSettings.css';

const config = require('../config.json');

class AppSettings extends React.Component {

    constructor(props) {
        super();
        this.state = {
            settings: {
                notify: props.app.settings.notify,
                storageTime: props.app.settings.storageTime,
                notificationLogLevel: props.app.settings.notificationLogLevel
            },
            deleteAppDialogOpen: false
        }
    }

    update(property, value) {
        this.setState({ [property]: value }, () => {
            const updatedApp = Object.assign({}, this.props.app, { settings: this.state.settings });
            axios.put(`${config.backendUrl}/api/applications/${this.props.app.id}`, updatedApp, {
                headers: {
                    authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then(resp => {
                    console.log('saved')
                })
                .catch(err => {
                    console.error(err);
                })
        });
    }

    openDeleteDialog = () => {
        this.setState({ deleteAppDialogOpen: true })
    }

    deleteApp = () => {
        axios.delete(`${config.backendUrl}/api/applications/${this.props.app.id}`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(resp => {
                console.log('deleted')
                this.props.onDelete();
                this.props.history.push('/dashboard');
            })
            .catch(err => {
                console.error(err);
            })
    }

    render() {
        return (
            <>
                <DataTable>
                    <DataTableContent>
                        <DataTableBody>
                            <DataTableRow>
                                <DataTableCell>Storage time (days):</DataTableCell>
                                <DataTableCell alignStart><Select className="select"
                                    options={[
                                        { label: '7 days', value: 7 },
                                        { label: '30 days', value: 30 },
                                        { label: '90 days', value: 90 }
                                    ]}
                                    defaultValue={this.props.app.settings.storageTime}
                                    onChange={evt => this.update('storageTime', parseInt(evt.target.value))}

                                /></DataTableCell>

                            </DataTableRow>
                            <DataTableRow>
                                <DataTableCell>Notification log level:</DataTableCell>
                                <DataTableCell alignStart><Select className="select"
                                    options={[
                                        { label: 'VERBOSE', value: 'VERBOSE' },
                                        { label: 'DEBUG', value: 'DEBUG' },
                                        { label: 'INFO', value: 'INFO' },
                                        { label: 'WARNING', value: 'WARNING' },
                                        { label: 'ERROR', value: 'ERROR' },
                                        { label: 'CRITICAL', value: 'CRITICAL' },
                                    ]}
                                    defaultValue={this.props.app.settings.notificationLogLevel}
                                    onChange={evt => this.update('notificationLogLevel', evt.target.value)}
                                /></DataTableCell>
                                <DataTableCell>E-mail notifications:</DataTableCell>
                                <DataTableCell>
                                    <Switch
                                        checked={!!this.state.settings.notify}
                                        onChange={evt => this.update('notify', evt.target.checked)}>
                                    </Switch>
                                </DataTableCell>
                            </DataTableRow>
                        </DataTableBody>
                    </DataTableContent>
                </DataTable>
                <Grid>
                    <GridCell span="4">
                        <Button
                            label="Delete App"
                            raised
                            onClick={this.openDeleteDialog}
                            theme={['secondaryBg', 'onSecondary']}
                        >Delete App</Button>
                    </GridCell>
                </Grid>
                <SimpleDialog
                    title="Delete app"
                    body="Are you sure you want to delete app and all its logs?"
                    open={this.state.deleteAppDialogOpen}
                    acceptLabel="Delete"
                    cancelLabel="Cancel"
                    onClose={evt => {
                        if (evt.detail.action === 'accept') {
                            this.deleteApp();
                        }
                        this.setState({ deleteAppDialogOpen: false })
                    }}
                />
            </>
        )
    }
}

export default AppSettings;